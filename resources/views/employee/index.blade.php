@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1>
        Employees
        <small>Employees Listing</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-Employees"></i> Home</a></li>
        <li class="active">Employees</li>
    </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-12">
        <div class="box box-default">
            <div class="box-body">
                <form class="form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Phone">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control datepicker" placeholder="Join Date">
                    </div>
                    <div class="form-group">
                        <select class="form-control">
                            <option value="">User Type</option>
                            <option value="1">Super Admin</option>
                            <option value="2">Sales Executive</option>
                            <option value="3">Operations Executive</option>
                        </select>
                    </div>
                    <div class="form-group" style="margin-left: 1.5em;">
                        <button type="submit" class="btn btn-xs bg-purple btn-flat margin"><i class="fa fa-search"></i> Search</button>
                        <button type="reset" class="btn btn-xs bg-maroon btn-flat margin">Clear</button>
                    </div>
                </form>
            </div>    
        </div>
        <div class="box">
            <div class="box-header">
                <a href="" class="btn bg-purple margin pull-right"><i class="fa fa-plus"></i> Add New</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Phone Number</th>
                  <th>Date of Birth</th>
                  <th>Action</th>
                </tr>
                <tr>
                  <td>Deep</td>
                  <td>Karmakar</td>
                  <td>deep.karmakar80@gmail.com</td>
                  <td>9007173746</td>
                  <td>02/07/1980</td>
                  <td>
                    <a href="" class="btn btn-xs bg-purple margin"><i class="fa fa-edit"></i> Edit</a>
                    <a href="" class="btn btn-xs bg-maroon margin"><i class="fa fa-trash"></i> Delete</a>
                  </td>
                </tr>
                <tr>
                  <td>Deep</td>
                  <td>Karmakar</td>
                  <td>deep.karmakar80@gmail.com</td>
                  <td>9007173746</td>
                  <td>02/07/1980</td>
                  <td>
                    <a href="" class="btn btn-xs bg-purple margin"><i class="fa fa-edit"></i> Edit</a>
                    <a href="" class="btn btn-xs bg-maroon margin"><i class="fa fa-trash"></i> Delete</a>
                  </td>
                </tr>
                <tr>
                  <td>Deep</td>
                  <td>Karmakar</td>
                  <td>deep.karmakar80@gmail.com</td>
                  <td>9007173746</td>
                  <td>02/07/1980</td>
                  <td>
                    <a href="" class="btn btn-xs bg-purple margin"><i class="fa fa-edit"></i> Edit</a>
                    <a href="" class="btn btn-xs bg-maroon margin"><i class="fa fa-trash"></i> Delete</a>
                  </td>
                </tr>
                <tr>
                  <td>Deep</td>
                  <td>Karmakar</td>
                  <td>deep.karmakar80@gmail.com</td>
                  <td>9007173746</td>
                  <td>02/07/1980</td>
                  <td>
                    <a href="" class="btn btn-xs bg-purple margin"><i class="fa fa-edit"></i> Edit</a>
                    <a href="" class="btn btn-xs bg-maroon margin"><i class="fa fa-trash"></i> Delete</a>
                  </td>
                </tr>
                <tr>
                  <td>Deep</td>
                  <td>Karmakar</td>
                  <td>deep.karmakar80@gmail.com</td>
                  <td>9007173746</td>
                  <td>02/07/1980</td>
                  <td>
                    <a href="" class="btn btn-xs bg-purple margin"><i class="fa fa-edit"></i> Edit</a>
                    <a href="" class="btn btn-xs bg-maroon margin"><i class="fa fa-trash"></i> Delete</a>
                  </td>
                </tr>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- ./col -->
    </div>
    <!-- /.row -->
    <!-- Main row -->
    </section>
    <!-- /.content -->
</div>
@endsection