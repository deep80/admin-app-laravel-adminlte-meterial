@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1>
        Employee
        <small>Add New Employee</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-Employees"></i> Employee</a></li>
        <li class="active">Add New</li>
    </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-12">
            <form action="" method="post" id="employee-create">
                @csrf
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <input name="firstname" placeholder="First Name" class="form-control" type="text">
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <input name="lastname" placeholder="Last Name" class="form-control" type="text">
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <input name="email" placeholder="Email Address" class="form-control" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <textarea name="address" placeholder="Address" class="form-control"></textarea>
                            </div>
                            <div class="col-xs-6">
                                <textarea name="alt-address" placeholder="Alternate Address" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <input name="firstname" placeholder="First Name" class="form-control" type="text">
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <input name="lastname" placeholder="Last Name" class="form-control" type="text">
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <input name="email" placeholder="Email Address" class="form-control" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn bg-purple margin"><i class="fa fa-save"></i> Save</button>
                <button type="reset" class="btn btn-default"><i class="fa fa-save"></i> Reset</button>
            </form>
        </div>
    </div>
    <!-- /.row -->
    <!-- Main row -->
    </section>
    <!-- /.content -->
</div>
@endsection