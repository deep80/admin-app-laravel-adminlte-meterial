$(function () {
    $('.datepicker').datepicker()
    $('[data-mask]').inputmask()
    $('.datemask').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
})