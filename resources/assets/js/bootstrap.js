
window._ = require('lodash');
window.Popper = require('popper.js').default;

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

require('jquery-validation')
require('../../MaterialAdminLTE/bower_components/jquery-ui/jquery-ui')
require('../../MaterialAdminLTE/bower_components/bootstrap/dist/js/bootstrap')
require('../../MaterialAdminLTE/dist/js/material')
require('../../MaterialAdminLTE/dist/js/ripples')
require('../../MaterialAdminLTE/bower_components/raphael/raphael')
require('../../MaterialAdminLTE/bower_components/jquery-knob/js/jquery.knob')
require('../../MaterialAdminLTE/bower_components/morris.js/morris')
require('../../MaterialAdminLTE/bower_components/jquery-sparkline/dist/jquery.sparkline')
require('../../MaterialAdminLTE/bower_components/moment/moment')
require('../../MaterialAdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll')
require('../../MaterialAdminLTE/bower_components/fastclick/lib/fastclick')
require('../../MaterialAdminLTE/dist/js/adminlte')
require('../../MaterialAdminLTE/dist/js/pages/dashboard')
require('../../MaterialAdminLTE/dist/js/demo')
require('../../MaterialAdminLTE/bower_components/ckeditor/ckeditor')
require('../../MaterialAdminLTE/bower_components/moment/moment')
require('../../MaterialAdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker')
require('../../MaterialAdminLTE/plugins/timepicker/bootstrap-timepicker')
require('../../MaterialAdminLTE/plugins/iCheck/icheck')
require('../../MaterialAdminLTE/plugins/input-mask/jquery.inputmask')
require('../../MaterialAdminLTE/plugins/input-mask/jquery.inputmask.extensions')
require('../../MaterialAdminLTE/plugins/input-mask/jquery.inputmask.date.extensions')
require('../../MaterialAdminLTE/plugins/input-mask/jquery.inputmask.phone.extensions')

require('./main')
require('./employee/create')

$.material.init();

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });
