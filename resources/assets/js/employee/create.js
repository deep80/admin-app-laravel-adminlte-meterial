$(function(){
    $('#employee-create').validate({
        rules: {
            firstname: {
                required: true
            },
            lastname: {
                required: true
            },
            email: {
                required: true,
                email: true,
            },
            address: {
                required: true,
            },
        },
        messages: {
            firstname: {
                required: 'Enter First Name'
            },
            lastname: {
                required: 'Enter Last Name'
            },
            email: {
                required: 'Enter Email Address',
                email: 'Must be a valid email address'
            },
            email: {
                required: 'Enter Residential Address'
            },
        }
    })
})